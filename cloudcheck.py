sr/bin/python
#
# Copyright 2015 Red Hat, Inc.
#
# Licensed under the GNU General Public License Version 2
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.


import paramiko
import requests

from libtaskotron import check

"""
This check is a demonstration check that I wrote quickly to test out an
implementation of Taskotron's disposable client feature. I explicitly didn't
use a proper runner for the steps because this is simple and doesn't require
any additional integration work.

For the love of pete, don't use this as an example or assume that this is how
checks in Taskotron will ever look. There are many things wrong with doing checks
this way and it is only meant as a quick-n-dirty demo
"""

class CloudCheck(object):

    def __init__(self, client_hostname):
        self.client_hostname = client_hostname

    def setup(self):
        self.ssh = paramiko.SSHClient()
        self.ssh.load_host_keys('/dev/null')
        self.ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())

    def teardown(self):
        if self.ssh is not None:
            self.ssh.close()

    def execute_ssh(self, command):
        self.ssh.connect(self.client_hostname, username='fedora')
        stdin, stdout, stderr = self.ssh.exec_command(command)

        output = (stdout.readlines(), stderr.readlines())
        self.ssh.close()
        return output

    def write_file(self, remote_path, data):
        self.ssh.connect(self.client_hostname, username='fedora')
        sftp = self.ssh.open_sftp()
        with sftp.open(remote_path, 'w+') as remote_file:
            remote_file.write(data)

    def install_nginx(self):
        cd = check.CheckDetail('cloudcheck: install nginx', check.ReportType.KOJI_BUILD)

        check_command = 'yum list installed nginx'
        output = self.execute_ssh(check_command)
        if output[1][0].strip() != u'Error: No matching Packages to list':
            print "cloudcheck:install_ngnix:      FAIL"
            cd.outcome = 'FAILED'
            cd.store("nginx already installed on client machine, check is invalid")
            return cd

        install_command = 'sudo yum -y install nginx'
        output = self.execute_ssh(install_command)

        output = self.execute_ssh(check_command)
        if output[1] == 'Error: No matching Packages to list':
            print "cloudcheck:install_ngnix:      FAIL"
            cd.outcome = 'FAILED'
            cd.store("nginx installation failed")
            return cd

        cd.outcome = 'PASSED'
        print "cloudcheck:install_ngnix:      PASS"
        return cd

    def start_nginx(self):
        cd = check.CheckDetail('cloudcheck: start nginx', check.ReportType.KOJI_BUILD)

        start_command = 'sudo systemctl start nginx'

        self.execute_ssh(start_command)
        output = self.execute_ssh('sudo systemctl status nginx | grep "acive (running)"')
        if output[0] == '':
            print "cloudcheck:start_ngnix:      FAIL"
            cd.outcome = 'FAILED'
            cd.store("nginx start failed")
            return cd

        cd.outcome = 'PASSED'
        print "cloudcheck:start_ngnix:      PASS"
        return cd


    def open_port80(self):
        cd = check.CheckDetail('cloudcheck: open port 80', check.ReportType.KOJI_BUILD)

        self.execute_ssh('sudo yum -y install system-config-firewall-base')

        output = self.execute_ssh('sudo yum list installed system-config-firewall-base')
        if output[1] == 'Error: No matching Packages to list':
            print "cloudcheck:open_port80:      FAIL"
            cd.outcome = 'FAILED'
            cd.store("lokkit installation failed")
            return cd

        self.execute_ssh('sudo lokkit -s http')
        output = self.execute_ssh('sudo grep "80" /etc/sysconfig/iptables')
        if output[0] != '-A INPUT -m state --state NEW -m tcp -p tcp --dport 80 -j ACCEPT':
            print "cloudcheck:open_port80:      FAIL"
            cd.outcome = 'FAILED'
            cd.store("opening port 80 failed")
            return cd

        cd.outcome = 'PASSED'
        print "cloudcheck:open_port80:      PASS"
        return cd

    def test_simple_http(self):
        cd = check.CheckDetail('cloudcheck: test_simple_http', check.ReportType.KOJI_BUILD)

        testfile_contents = 'this is just a test'
        self.write_file('/home/fedora/index.html', testfile_contents)
        self.execute_ssh('sudo mv /home/fedora/index.html /usr/share/nginx/html/index.html')

        r = requests.get('http://{}/'.format(self.client_hostname))
        if not r.ok and r.text == testfile_contents:
            print "cloudcheck:test_simple_http:      FAIL"
            cd.outcome = 'FAILED'
            cd.store("content retrieval failed")
            return cd

        cd.outcome = 'PASSED'
        print "cloudcheck:test_simple_http:      PASS"
        return cd

def run(workdir=None, client_ip=None):
    if client_ip is None:
        raise Exception("need a client IP in order for test suite to run")

    results = []
    try:
        print "setting up test suite"
        cloudcheck = CloudCheck(client_ip)
        cloudcheck.setup()

        # start running checks
        results.append(cloudcheck.install_nginx())
        results.append(cloudcheck.start_nginx())
        results.append(cloudcheck.test_simple_http())


    except Exception:
        print "some failure"
        raise

    return check.export_TAP(results, checkname='cloudcheck')
